//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"


/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */

float Wallis_Float(){
    float piApprox = 1;
    for(float i = 1; i < 100000; i++){
        piApprox *= (4*i*i)/((4*i*i) - 1) ;
    }
    return piApprox * 2;
}

double Wallis_Double(){
    double piApprox = 1;
    for(double i = 1; i < 100000; i++){
        piApprox *= (4*i*i)/((4*i*i) - 1) ;
    }
    return piApprox * 2;
}

int main() {

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif

    // Float calculations and printing
    const float PI_EST_FLOAT = 3.14159265359;
    float myPiEstFloat = Wallis_Float();
    float myAbsErrorFloat = PI_EST_FLOAT - myPiEstFloat;

    printf("%f is my pi float estimate. \n", myPiEstFloat);
    printf("%f is my absolute error\n", myAbsErrorFloat);


    //Double calculations and printing
    const double PI_EST_DOUBLE = 3.14159265359;
    double myPiEstDouble = Wallis_Double();
    float myAbsErrorDouble =PI_EST_DOUBLE - myPiEstDouble;

    printf("%f is my pi double estimate.\n", myPiEstDouble);
    printf("%f is my absolute error\n", myAbsErrorDouble);

    // Returning zero indicates everything went okay.
    return 0;
}